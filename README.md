# SQL Workbench/J

This is the public Git repository for SQL Workbench/J

Homepage and downloads are here: http://www.sql-workbench.eu

License information can be found at: https://www.sql-workbench.eu/manual/license.html

The maintainer of this project endorses the following values:

* human rights
* diversity
* inclusiveness
* social justice
* equality

The maintainer of this project despises the following:

* racism
* fascism
* inequality
